package com.messenger.demo.models;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class LoginUser {

    private  int id;

    @NotEmpty(message = "User's name cannot be empty.")
    @Size(min = 5, max = 250)
    private String name;

    @NotEmpty(message = "User's password cannot be empty.")
    @Size(min = 3)
    private String password;
}
