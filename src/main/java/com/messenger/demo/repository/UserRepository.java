package com.messenger.demo.repository;

import com.messenger.demo.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByName(@NotEmpty(message = "User's name cannot be empty.") @Size(min = 5, max = 250) String name);
    User save();

    User findByUsername(String username);
}
