package com.messenger.demo.service;

import com.messenger.demo.models.User;

import lombok.Data;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Data
@Component
public class UsersDTO {

    private final JdbcTemplate jdbcTemplate;

    public void addUser(User user){
        jdbcTemplate.update("INSERT INTO users(name, password, email) VALUES (?,?,?)",
                user.getName(),user.getPassword(),user.getEmail());
    }
    public void deletedUser(int id){
        jdbcTemplate.update("DELETE FROM users where id = ?",id);
    }

    public boolean checkUser(String name) {
        return jdbcTemplate.query("SELECT * FROM users WHERE name = ?",
                new Object[]{name},
                new UsersMapper())
                .stream()
                .filter(user -> name.equals(user.getName()))
                .findFirst()
                .stream().anyMatch(user -> user.getName().equals(name));
    }

    public List<User> showAllUsers(){
        return jdbcTemplate.query("SELECT * FROM users",new UsersMapper());
    }
}

