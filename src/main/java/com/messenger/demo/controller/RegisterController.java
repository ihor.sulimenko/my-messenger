package com.messenger.demo.controller;


import com.messenger.demo.models.LoginUser;
import com.messenger.demo.models.User;
import com.messenger.demo.service.UsersDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class RegisterController {
    private final UsersDTO usersDTO;
    public RegisterController(UsersDTO usersDTO) {
        this.usersDTO = usersDTO;
    }

    @GetMapping("/register")
    public String register(Model model){
        model.addAttribute("user",new User());
        return "registration";
    }

    @PostMapping("/register")
    public String creatNewUser(@Valid @ModelAttribute("user")User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "registration";
        }
        usersDTO.addUser(user);
        return "/register/login";
    }
    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("user",new User());
        return "/register/login";
    }

    @PostMapping("/login")
    public String authenticationUser(@Valid @ModelAttribute("user") LoginUser user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "redirect:/register/login";
        }
        if(!usersDTO.checkUser(user.getName())){
            return "redirect:/register/login";
        }
        return "/main/mainpage";
    }
}
