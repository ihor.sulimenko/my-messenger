package com.messenger.demo.controller;


import com.messenger.demo.models.User;
import com.messenger.demo.service.UsersDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {
    private final UsersDTO usersDTO;

    public MainController(UsersDTO usersDTO) {
        this.usersDTO = usersDTO;
    }

    @GetMapping("/main")
    public String mainPage(){
        return "main/mainpage";
    }

    @GetMapping("/userslist")
    public String listUsers(@ModelAttribute("user") User user,Model model){
        model.addAttribute("users",usersDTO.showAllUsers());
        return "/main/userslist";
    }

    @GetMapping("profile")
    public String profileUser(@ModelAttribute("user") User user){
        return "main/profile";
    }
}
